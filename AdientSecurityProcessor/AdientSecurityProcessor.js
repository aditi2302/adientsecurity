'use strict'

var HashMap = require('hashmap');
var c_sessionMap = new HashMap();

var ConfigurationManager = require('../../PlatformAPI/ConfigurationManager/ConfigurationManager.js').ConfigurationManager;
var c_configurationmanager =  new ConfigurationManager();

//Load Entity Status Manager
var EntityStatusManager = require('../../PlatformAPI/EntityStatusManager/EntityStatusManager.js').EntityStatusManager;
var c_entitystatusmanager = new EntityStatusManager();

//Loading EnterpriseEntityLogger
var EnterpriseEntityLogger = require('../../PlatformAPI/EnterpriseEntityLogger/EnterpriseEntityLogger.js').EnterpriseEntityLogger;
var c_enterpriseentitylogger = new EnterpriseEntityLogger();

//Load Entity states enum
var ComponentStatus = require('../../PlatformAPI/Utilities/ComponentStatus.js').ComponentStatus;

var c_pretty = require('pretty-time');
var c_entityname='AdientSecurityProcessor';
var c_logger = require('../../PlatformAPI/Utilities/Logger.js')(module);
var c_instancename='';

class AdientSecurityProcessor{

	constructor(p_instancename){
		c_instancename = p_instancename;
		c_entitystatusmanager.updateEntityState(p_instancename,c_entityname,ComponentStatus.STARTED,function(p_statusmessage){
			c_logger.info('Started '+c_entityname);
			c_enterpriseentitylogger.insertEntityLog(p_instancename,c_entityname,'Adient Security Processor started','ENTITY_STATE',function(success){
				c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+c_entityname+"):"+success);
			});	
		});		
	}
	
	manageProcessing(p_commonobject, p_airesponse, p_language, c_aiinstance2, orchestratorcallback) {
		if(p_airesponse!=null && p_airesponse!='' && p_airesponse!=undefined){
			if(p_commonobject!=null && p_commonobject!='' && p_commonobject!=undefined){	
				try{
					c_logger.info("Inside manageProcessing");
					//var l_instancename=p_commonobject.instancename;
					c_enterpriseentitylogger.insertEntityLog(c_instancename,c_entityname,'Action to be performed:'+p_airesponse.action,'DEBUG',function(success){
						c_logger.info("Entity log Inserted for (instance:"+c_instancename+", entity:"+c_entityname+"):"+success);
					});
					
					c_logger.debug('Action to be performed:'+p_airesponse.action);
							
					var l_response;
					
					var l_currentSession;
					
					var l_getSession = function() {
						if (null != c_sessionMap.get(p_airesponse.sessionId + "")) {
							c_logger.info("session already exist.");
							l_currentSession = c_sessionMap.get(p_airesponse.sessionId + "");
							return l_currentSession;
						} else {
							l_currentSession = new Object();
							c_logger.info("Currently session does not exist.");
							l_currentSession.issueDescription = '';
							l_currentSession.issueCategory = '';
							l_currentSession.criticalJustification = '';
							l_currentSession.issueUrgency = '';
							c_sessionMap.set(p_airesponse.sessionId + "", l_currentSession);
							return l_currentSession;
						}
					}
					
					 if(p_airesponse.action === 'Kieper_cl_action')
					{
						var l_currentSession = l_getSession();
						if( l_currentSession.issueDescription != null && l_currentSession.issueDescription != '' )	{
							l_currentSession.issueDescription = l_currentSession.issueDescription +' '+ p_airesponse.parameters.any;
							c_logger.debug("+++++++++++++++++++"+l_currentSession.issueDescription);
						}	else	{
							c_sessionMap.set(p_airesponse.sessionId + "", l_currentSession);
							l_currentSession.issueDescription = p_airesponse.parameters.any;
							c_logger.debug("+++++++++++++++++++"+l_currentSession.issueDescription);
						}
						l_response = 'Okay.Try unlocking your account using the link http://sapapps.autoexpr.com and check if you are able to login'+ p_airesponse.responseText;
						c_logger.debug('Response to be sent to orchestrator: ' + l_response);
						orchestratorcallback(l_response);
					}
					else if(p_airesponse.action === 'Recaro_client_action')
					{
						var l_currentSession = l_getSession();
						if( l_currentSession.issueDescription != null && l_currentSession.issueDescription != '' )	{
							l_currentSession.issueDescription = l_currentSession.issueDescription +' '+ p_airesponse.parameters.any;
							c_logger.debug("+++++++++++++++++++"+l_currentSession.issueDescription);
						}	else	{
							c_sessionMap.set(p_airesponse.sessionId + "", l_currentSession);
							l_currentSession.issueDescription = p_airesponse.parameters.any;
							c_logger.debug("+++++++++++++++++++"+l_currentSession.issueDescription);
						}
						l_response = 'Okay.Try unlocking your account using the link http://sapapps.autoexpr.com and check if you are able to login'+ p_airesponse.responseText;
						c_logger.debug('Response to be sent to orchestrator: ' + l_response);
						orchestratorcallback(l_response);
					}
					else if(p_airesponse.action === 'crhprd_client_action')
					{
						var l_currentSession = l_getSession();
						if( l_currentSession.issueDescription != null && l_currentSession.issueDescription != '' )	{
							l_currentSession.issueDescription = l_currentSession.issueDescription +' '+ p_airesponse.parameters.any;
							c_logger.debug("+++++++++++++++++++"+l_currentSession.issueDescription);
						}	else	{
							c_sessionMap.set(p_airesponse.sessionId + "", l_currentSession);
							l_currentSession.issueDescription = p_airesponse.parameters.any;
							c_logger.debug("+++++++++++++++++++"+l_currentSession.issueDescription);
						}
						l_response = 'Okay.Try unlocking your account using the link http://sapapps.autoexpr.com and check if you are able to login'+ p_airesponse.responseText;
						c_logger.debug('Response to be sent to orchestrator: ' + l_response);
						orchestratorcallback(l_response);
					}
					else if (p_airesponse.action === 'detailed_decription_for_unlocking_action')
					{
						var l_currentSession = l_getSession();
						if( l_currentSession.issueDescription != null && l_currentSession.issueDescription != '' )	{
							l_currentSession.issueDescription = l_currentSession.issueDescription +' '+ p_airesponse.parameters.any;
							c_logger.debug("+++++++++++++++++++"+l_currentSession.issueDescription);
						}	else	{
							c_sessionMap.set(p_airesponse.sessionId + "", l_currentSession);
							l_currentSession.issueDescription = p_airesponse.parameters.any;
							c_logger.debug("+++++++++++++++++++"+l_currentSession.issueDescription);
						}
						l_response = 'Okay.Try unlocking your account using the link http://sapapps.autoexpr.com and check if you are able to login'+ p_airesponse.responseText;
						c_logger.debug('Response to be sent to orchestrator: ' + l_response);
						orchestratorcallback(l_response);			
					}
						
					else if(p_airesponse.action === "admin_lock_action")
					{
						if(p_airesponse.parameters.admin != null && p_airesponse.parameters.admin != "" || p_airesponse.parameters.lock != null && p_airesponse.parameters.lock !=null)
						{
						var l_currentSession = l_getSession();
						if( l_currentSession.issueDescription != null && l_currentSession.issueDescription != '' )	{
							l_currentSession.issueDescription = l_currentSession.issueDescription +' '+ p_airesponse.parameters.any;
							c_logger.debug("+++++++++++++++++++"+l_currentSession.issueDescription);
						}	else	{
							c_sessionMap.set(p_airesponse.sessionId + "", l_currentSession);
							l_currentSession.issueDescription = p_airesponse.parameters.any;
							c_logger.debug("+++++++++++++++++++"+l_currentSession.issueDescription);
						}
						l_response = 'Okay.Kindly send an email to AE-SAPCOE-ServiceDesk-EU@adient.com.Our SAPCOE ServiceDesk Team will unlock your account and provide resolution.'+ p_airesponse.responseText;
						c_logger.debug('Response to be sent to orchestrator: ' + l_response);
						orchestratorcallback(l_response);
						}
						else
						{
							l_response='I didn\'t get what you are trying to say,Kindly rephrase your query.';
							c_logger.debug('Response to be sent to orchestrator: ' + l_response);
							orchestratorcallback(l_response);
						}	
					}
			   
						
					else {
					  
						l_response = p_airesponse.responseText;
						c_logger.debug('Response to be sent to orchestrator: ' + l_response);
						orchestratorcallback(l_response);
					  
					}	  	  
				}
				catch(exception){
					c_logger.error("Error occurred :: Class - AdientSecurityProcessor :: Method - manageProcessing() :: "+exception);	
				}
			}
			else{
				c_logger.error("Error occurred :: Class - AdientSecurityProcessor :: Method - manageProcessing() :: common object not defined.");	
			}
		}
		else{
			c_logger.error("Error occurred :: Class - AdientSecurityProcessor :: Method - manageProcessing() :: ai response not defined.");
		}
	}	
}
module.exports = AdientSecurityProcessor;